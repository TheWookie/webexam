package com.wookie.dao;

import com.wookie.entity.Question;

/**
 * Created by wookie on 6/15/17.
 */
public interface QuestionDao extends GenericDao<Question> {

}
