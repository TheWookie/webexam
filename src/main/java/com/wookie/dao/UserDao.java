package com.wookie.dao;

import com.wookie.entity.User;

import javax.persistence.NoResultException;

/**
 * Created by wookie on 6/15/17.
 */
public interface UserDao extends GenericDao<User> {
    User read(String login) throws NoResultException;
}
