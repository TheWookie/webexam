package com.wookie.dao;

import com.wookie.entity.Test;

/**
 * Created by wookie on 6/15/17.
 */
public interface TestDao extends GenericDao<Test> {

}
