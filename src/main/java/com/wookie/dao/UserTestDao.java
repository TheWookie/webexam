package com.wookie.dao;

import com.wookie.entity.UserTest;

import javax.persistence.NoResultException;

/**
 * Created by wookie on 6/15/17.
 */
public interface UserTestDao extends GenericDao<UserTest> {
//    public UserTest read(int userId, int testId) throws NoResultException;
}
