package com.wookie.dao.impl;

import com.wookie.dao.UserDao;
import com.wookie.entity.User;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User create(User entity) {
        try {
            entityManager.persist(entity);
            return entity;
        } catch(Exception e) {
            throw new RuntimeException("Such user is already exists.");
        }
    }

    @Override
    public User read(User entity) throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);

        cq.where(cb.equal(root.get("id"), entity.getId()));
        TypedQuery<User> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

    @Override
    public User read(String login) throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);

        cq.where(cb.equal(root.get("login"), login));
        TypedQuery<User> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

    @Override
    public List<User> readAll() throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);

        cq.select(root);

        TypedQuery<User> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    @Override
    public User update(User entity) {
        return entityManager.merge(entity);
    }

    @Override
    public boolean delete(User entity) {
        User user = read(entity);
        entityManager.remove(user);
        return true;
    }

}
