package com.wookie.dao.impl;

import com.wookie.dao.UserTestDao;
import com.wookie.entity.Test;
import com.wookie.entity.User;
import com.wookie.entity.UserTest;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserTestDaoImpl implements UserTestDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UserTest create(UserTest entity) {
        try {
            entityManager.persist(entity);
            return entity;
        } catch(Exception e) {
            throw new RuntimeException("Such user is already exists.");
        }
    }

    @Override
    public UserTest read(UserTest entity) throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserTest> cq = cb.createQuery(UserTest.class);
        Root<UserTest> root = cq.from(UserTest.class);

        cq.where(cb.equal(root.get("user"), new User(entity.getUser().getId())))
                .where(cb.equal(root.get("test"), new Test(entity.getTest().getId())));

        TypedQuery<UserTest> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

//    @Override
//    public UserTest read(int userId, int testId) throws NoResultException {
//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<UserTest> cq = cb.createQuery(UserTest.class);
//        Root<UserTest> root = cq.from(UserTest.class);
//
//        cq.where(cb.equal(root.get("user"), new User(userId)))
//            .where(cb.equal(root.get("test"), new Test(testId)));
//
//        TypedQuery<UserTest> q = entityManager.createQuery(cq);
//        return q.getSingleResult();
//    }

    @Override
    public List<UserTest> readAll() throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserTest> cq = cb.createQuery(UserTest.class);
        Root<UserTest> root = cq.from(UserTest.class);

        cq.select(root);

        TypedQuery<UserTest> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    @Override
    public UserTest update(UserTest entity) {
        return entityManager.merge(entity);
    }

    @Override
    public boolean delete(UserTest entity) {
        UserTest userTest = read(entity);
        entityManager.remove(userTest);
        return true;
    }

}
