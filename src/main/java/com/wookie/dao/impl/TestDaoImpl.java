package com.wookie.dao.impl;

import com.wookie.dao.TestDao;
import com.wookie.entity.Test;
import com.wookie.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class TestDaoImpl implements TestDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Test create(Test entity) {
        try {
            entityManager.persist(entity);
            return entity;
        } catch(Exception e) {
            throw new RuntimeException("Such user is already exists.");
        }
    }

    @Override
    public Test read(Test entity) throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Test> cq = cb.createQuery(Test.class);
        Root<Test> root = cq.from(Test.class);

        cq.where(cb.equal(root.get("id"), entity.getId()));
        TypedQuery<Test> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

    @Override
    public List<Test> readAll() throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Test> cq = cb.createQuery(Test.class);
        Root<Test> root = cq.from(Test.class);

        cq.select(root);

        TypedQuery<Test> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    @Override
    public Test update(Test entity) {
        return entityManager.merge(entity);
    }

    @Override
    public boolean delete(Test entity) {
        Test test = read(entity);
        entityManager.remove(test);
        return true;
    }
}
