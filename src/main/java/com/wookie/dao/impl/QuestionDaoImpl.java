package com.wookie.dao.impl;

import com.wookie.dao.QuestionDao;
import com.wookie.entity.Question;
import com.wookie.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class QuestionDaoImpl implements QuestionDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Question create(Question entity) {
        try {
            entityManager.persist(entity);
            return entity;
        } catch(Exception e) {
            throw new RuntimeException("Such user is already exists.");
        }
    }

    @Override
    public Question read(Question entity) throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Question> cq = cb.createQuery(Question.class);
        Root<Question> root = cq.from(Question.class);

        cq.where(cb.equal(root.get("id"), entity.getId()));
        TypedQuery<Question> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

    @Override
    public List<Question> readAll() throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Question> cq = cb.createQuery(Question.class);
        Root<Question> root = cq.from(Question.class);

        cq.select(root);

        TypedQuery<Question> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    @Override
    public Question update(Question entity) {
        return entityManager.merge(entity);
    }

    @Override
    public boolean delete(Question entity) {
        Question question = read(entity);
        entityManager.remove(question);
        return true;
    }
}
