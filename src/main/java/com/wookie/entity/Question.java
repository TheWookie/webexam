package com.wookie.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * Question generated by hbm2java
 */
@Entity
public class Question implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	private Test test;
	private String title;
	private String description;
	@Column(name="several_answers")
	private boolean severalAnswers;
	private int order;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<Answer> answers = new HashSet<>(0);

	public Question() {
	}

	public Question(int id) {
		this.id = id;
	}

	public Question(Test test, String title, boolean severalAnswers, int order) {
		this.test = test;
		this.title = title;
		this.severalAnswers = severalAnswers;
		this.order = order;
	}

	public Question(Test test, String title, String description, boolean severalAnswers, int order, Set answers) {
		this.test = test;
		this.title = title;
		this.description = description;
		this.severalAnswers = severalAnswers;
		this.order = order;
		this.answers = answers;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Test getTest() {
		return this.test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSeveralAnswers() {
		return this.severalAnswers;
	}

	public void setSeveralAnswers(boolean severalAnswers) {
		this.severalAnswers = severalAnswers;
	}

	public int getOrder() {
		return this.order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public Set getAnswers() {
		return this.answers;
	}

	public void setAnswers(Set answers) {
		this.answers = answers;
	}

}
