package com.wookie.service;

import com.wookie.dao.AnswerDao;
import com.wookie.dao.QuestionDao;
import com.wookie.entity.Answer;
import com.wookie.entity.Question;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class QuestionService {

    @Autowired
    private QuestionDao questionDao;

    @Transactional(readOnly = true)
    public List<Question> readAll() {
        try {
            return questionDao.readAll();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public Question read(int id) {
        try {
            return questionDao.read(new Question(id));
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public Question create(Question user) {
        try {
            return questionDao.create(user);
        } catch (RuntimeException e) {
            return null;
        }
    }

    @Transactional
    public boolean delete(int id) {
        try {
            return questionDao.delete(new Question(id));
        } catch(ConstraintViolationException e) {
            return false;
        }
    }
}
