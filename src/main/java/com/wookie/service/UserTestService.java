package com.wookie.service;

import com.wookie.dao.UserTestDao;
import com.wookie.entity.Test;
import com.wookie.entity.User;
import com.wookie.entity.UserTest;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class UserTestService {

    @Autowired
    private UserTestDao userTestDao;

    @Transactional(readOnly = true)
    public UserTest read(int userId, int testId) {
        UserTest userTest = new UserTest(new Test(testId), new User(userId));

        try {
            return userTestDao.read(userTest);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public UserTest create(int userId, int testId, int score) {
        UserTest userTest = new UserTest(new Test(testId), new User(userId), score);

        return userTestDao.create(userTest);
    }

    @Transactional
    public UserTest update(int userId, int testId, int newScore) {
        UserTest userTest = new UserTest(new Test(testId), new User(userId), newScore);

        try {
            return userTestDao.update(userTest);
        } catch(ConstraintViolationException e) {
            return null;
        }
    }

    @Transactional
    public boolean delete(int userId, int testId) {
        UserTest userTest = new UserTest(new Test(testId), new User(userId));

        try {
            return userTestDao.delete(userTest);
        } catch(ConstraintViolationException e) {
            return false;
        }
    }
}
