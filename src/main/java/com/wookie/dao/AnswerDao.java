package com.wookie.dao;

import com.wookie.entity.Answer;

/**
 * Created by wookie on 6/15/17.
 */
public interface AnswerDao extends GenericDao<Answer> {

}
