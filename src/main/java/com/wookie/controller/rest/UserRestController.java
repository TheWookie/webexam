package com.wookie.controller.rest;

import com.wookie.entity.User;
import com.wookie.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController(value = "/users")
public class UserRestController {
    private final Logger logger = LogManager.getLogger(this);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public ResponseEntity<User> create(@RequestParam("user") User user) {
        return (user != null)
                ? new ResponseEntity<>(userService.create(user), HttpStatus.CREATED)
                : new ResponseEntity<>((User) null, HttpStatus.NOT_ACCEPTABLE);
    }

    @RequestMapping(value = "/{userId}")
    public ResponseEntity<User> getById(@PathVariable("userId") int userId) {
        User user = userService.read(userId);
        return (user != null)
                ? new ResponseEntity<>(user, HttpStatus.FOUND)
                : new ResponseEntity<>((User) null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "")
    public ResponseEntity<List<User>> getAll() {
        return new ResponseEntity<>(userService.readAll(), HttpStatus.OK);
    }
}
