package com.wookie.service;

import com.wookie.dao.UserDao;
import com.wookie.entity.User;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Transactional(readOnly = true)
    public List<User> readAll() {
        try {
            return userDao.readAll();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public User read(int id) {
        try {
            return userDao.read(new User(id));
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public User read(String login) {
        try {
            return userDao.read(login);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public User create(User user) {
        try {
            return userDao.create(user);
        } catch (RuntimeException e) {
            return null;
        }
    }

    @Transactional
    public boolean delete(int id) {
        try {
            return userDao.delete(new User(id));
        } catch(ConstraintViolationException e) {
            return false;
        }
    }
}
