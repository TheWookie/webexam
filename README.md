Database script.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `webexamdb` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `webexamdb`.`User` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `fullname` VARCHAR(45) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `webexamdb`.`Test` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(345) NOT NULL,
  `duration` INT(11) NOT NULL,
  `pass_score` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `webexamdb`.`Question` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(345) NULL DEFAULT NULL,
  `several_answers` TINYINT(1) NOT NULL,
  `order` INT(11) NOT NULL,
  `test_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Question_1_idx` (`test_id` ASC),
  CONSTRAINT `fk_Question_1`
    FOREIGN KEY (`test_id`)
    REFERENCES `webexamdb`.`Test` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `webexamdb`.`Answer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(100) NOT NULL,
  `order` INT(11) NOT NULL,
  `correct` TINYINT(1) NOT NULL,
  `question_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Answer_1_idx` (`question_id` ASC),
  CONSTRAINT `fk_Answer_1`
    FOREIGN KEY (`question_id`)
    REFERENCES `webexamdb`.`Question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `webexamdb`.`UserTest` (
  `user_id` INT(11) NOT NULL,
  `test_id` INT(11) NOT NULL,
  `score` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `test_id`),
  INDEX `fk_User_has_Test_Test1_idx` (`test_id` ASC),
  INDEX `fk_User_has_Test_User1_idx` (`user_id` ASC),
  CONSTRAINT `fk_User_has_Test_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `webexamdb`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Test_Test1`
    FOREIGN KEY (`test_id`)
    REFERENCES `webexamdb`.`Test` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;