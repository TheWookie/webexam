package com.wookie.dao;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by wookie on 6/15/17.
 */
public interface GenericDao<T> {

    T create(T entity);

    T read(T entity) throws NoResultException;

    List<T> readAll() throws NoResultException;

    T update(T entity);

    boolean delete(T entity);

}
