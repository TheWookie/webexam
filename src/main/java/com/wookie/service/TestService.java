package com.wookie.service;

import com.wookie.dao.AnswerDao;
import com.wookie.dao.TestDao;
import com.wookie.entity.Answer;
import com.wookie.entity.Test;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class TestService {

    @Autowired
    private TestDao testDao;

    @Transactional(readOnly = true)
    public List<Test> readAll() {
        try {
            return testDao.readAll();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public Test read(int id) {
        try {
            return testDao.read(new Test(id));
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public Test create(Test user) {
        try {
            return testDao.create(user);
        } catch (RuntimeException e) {
            return null;
        }
    }

    @Transactional
    public boolean delete(int id) {
        try {
            return testDao.delete(new Test(id));
        } catch(ConstraintViolationException e) {
            return false;
        }
    }
}
