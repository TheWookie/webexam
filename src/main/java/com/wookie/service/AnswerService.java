package com.wookie.service;

import com.wookie.dao.AnswerDao;
import com.wookie.dao.UserDao;
import com.wookie.entity.Answer;
import com.wookie.entity.User;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class AnswerService {

    @Autowired
    private AnswerDao answerDao;

    @Transactional(readOnly = true)
    public List<Answer> readAll() {
        try {
            return answerDao.readAll();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public Answer read(int id) {
        try {
            return answerDao.read(new Answer(id));
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public Answer create(Answer user) {
        try {
            return answerDao.create(user);
        } catch (RuntimeException e) {
            return null;
        }
    }

    @Transactional
    public boolean delete(int id) {
        try {
            return answerDao.delete(new Answer(id));
        } catch(ConstraintViolationException e) {
            return false;
        }
    }
}
