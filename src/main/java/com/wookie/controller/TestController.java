package com.wookie.controller;

import com.wookie.entity.User;
import com.wookie.service.TestService;
import com.wookie.service.UserService;
import com.wookie.service.UserTestService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class TestController {
    private final Logger logger = LogManager.getLogger(this);

    @Autowired
    UserService userService;

    @Autowired
    UserTestService userTestService;

    @Autowired
    TestService testService;

    @RequestMapping(value = "/{jobId}")
    public String test(@PathVariable("jobId") Integer jobId, Model model) {
        model.addAttribute("id", userService.readAll().get(0).getFullname());
//        model.addAttribute("id", userService.readAll().get(0).getFullname());

        System.out.println(userService.readAll());

        return "index";
    }

    @RequestMapping(value = "/users/{userId}")
    public String test2(@PathVariable("userId") Integer userId, Model model) {
        model.addAttribute("id", userService.read(userId).getLogin());
//        model.addAttribute("id", userService.read("log1").getPassword());

        return "index";
    }

    @RequestMapping(value = "/create")
    public String test4() {
        userService.create(new User("d", "e", "f"));

        return "index";
    }

    @RequestMapping(value = "/delete/{id}")
    public String test3(@PathVariable("id") Integer id) {
        userService.delete(id);

        return "index";
    }

    @RequestMapping(value = "user-test/{userId}/{testId}")
    public String test5(
            @PathVariable("userId") Integer userId,
            @PathVariable("testId") Integer testId,
            Model model) {

        userTestService.update(userId, testId, 11);

        model.addAttribute("id", userTestService.read(userId, testId).getScore());

        return "index";
    }

    @RequestMapping(value = "user-test/delete/{userId}/{testId}")
    public String test6(
            @PathVariable("userId") Integer userId,
            @PathVariable("testId") Integer testId,
            Model model) {

        userTestService.delete(userId, testId);

        model.addAttribute("id", userTestService.read(userId, testId).getScore());

        return "index";
    }

    @RequestMapping(value = "/tests/{testId}")
    public String test7(@PathVariable("testId") Integer testId, Model model) {
        model.addAttribute("id", testService.read(testId).getQuestions());

        return "index";
    }

    @RequestMapping(value = "/tests/delete/{testId}")
    public String test8(@PathVariable("testId") Integer testId, Model model) {
        model.addAttribute("id", testService.delete(testId));

        return "index";
    }

}
