package com.wookie.dao.impl;

import com.wookie.dao.AnswerDao;
import com.wookie.entity.Answer;
import com.wookie.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AnswerDoaImpl implements AnswerDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Answer create(Answer entity) {
        try {
            entityManager.persist(entity);
            return entity;
        } catch(Exception e) {
            throw new RuntimeException("Such user is already exists.");
        }
    }

    @Override
    public Answer read(Answer entity) throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Answer> cq = cb.createQuery(Answer.class);
        Root<Answer> root = cq.from(Answer.class);

        cq.where(cb.equal(root.get("id"), entity.getId()));
        TypedQuery<Answer> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

    @Override
    public List<Answer> readAll() throws NoResultException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Answer> cq = cb.createQuery(Answer.class);
        Root<Answer> root = cq.from(Answer.class);

        cq.select(root);

        TypedQuery<Answer> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    @Override
    public Answer update(Answer entity) {
        return entityManager.merge(entity);
    }

    @Override
    public boolean delete(Answer entity) {
        Answer answer = read(entity);
        entityManager.remove(answer);
        return true;
    }
}
